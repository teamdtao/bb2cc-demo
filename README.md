---
page_id: 94142483
title: Bitbucket to Confluence Cloud
---

This page is intended as a demo of the teamdtao/bb2cc library.

## Features

In addition to [standard Markdown syntax][1], this library has built-in support
for certain Confluence features.

### Autolinks

Links to other Confluence pages are automatically rendered as inline cards.

For example, below is a link to the Confluence page that is backed by this
README file. It will be rendered in Confluence as an inline card.

https://taosoft.atlassian.net/wiki/spaces/TAO/pages/94142483/Bitbucket+to+Confluence+Cloud

### User mentions

Mention a user by putting a `@` before their nickname. For example, here I will
mention myself: @Dan

### Tables

Tables are supported using [extended Markdown syntax][2]. The below should
render as a table in Confluence.

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |

### Panels

The syntax for rendering an info panel is the same as for rendering a [fenced
code block][3], but with `panel:<type>` as the "language" for the block.

```panel:info
This should be rendered as an info panel in Confluence.
```

### Status lozenges

You can render a [[Status Lozenge:Green]] in Confluence with `[[` and `]]`.

### Task lists

The following will render as a normal list in Bitbucket but in Confluence it
will appear as a task list.

- [x] Completed task
- [ ] Incomplete task

[1]: https://www.markdownguide.org/basic-syntax
[2]: https://www.markdownguide.org/extended-syntax/#tables
[3]: https://www.markdownguide.org/extended-syntax/#fenced-code-blocks
